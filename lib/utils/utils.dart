import 'package:flutter/material.dart';
import 'package:registro_personal_cidenet/utils/api/server.response.dart';
import 'package:registro_personal_cidenet/widgets/dropdown/dropdown.widget.dart';

hideKeyBoard(BuildContext context) {
  FocusScope.of(context).requestFocus(FocusNode());
}

List<Item> get countryList => [
      Item(name: 'Colombia', value: 'Colombia'),
      Item(name: 'Estados Unidos', value: 'Estados Unidos'),
    ];

List<Item> get idTypeList => [
      Item(name: 'Cédula de ciudadanía', value: 'Cédula de ciudadanía'),
      Item(name: 'Cédula de extranjería', value: 'Cédula de extranjería'),
      Item(name: 'Pasaporte', value: 'Pasaporte'),
      Item(name: 'Permiso especial', value: 'Permiso especial'),
    ];

List<Item> get areaList => [
      Item(name: 'Administración', value: 'Administración'),
      Item(name: 'Financiera', value: 'Financiera'),
      Item(name: 'Compras', value: 'Compras'),
      Item(name: 'Infraestructura', value: 'Infraestructura'),
      Item(name: 'Operación', value: 'Operación'),
      Item(name: 'Talento Humano', value: 'Talento Humano'),
      Item(name: 'Servicios varios', value: 'Servicios varios'),
    ];

String convertDDMMAAAAHHMMSS(DateTime dateTime) {
  return dateTime is DateTime
      ? "${dateTime.day.toString().padLeft(2, '0')}-${dateTime.month.toString().padLeft(2, '0')}-${dateTime.year.toString().padLeft(4, '0')} ${dateTime.hour.toString().padLeft(2, '0')}:${dateTime.minute.toString().padLeft(2, '0')}:${dateTime.second.toString().padLeft(2, '0')}"
      : null;
}

String convertAAAAMMDD(DateTime dateTime) {
  return dateTime is DateTime
      ? "${dateTime.year.toString().padLeft(4, '0')}-${dateTime.month.toString().padLeft(2, '0')}-${dateTime.day.toString().padLeft(2, '0')}"
      : null;
}

String convertAAAAMMDDTZ(DateTime dateTime) {
  return dateTime is DateTime
      ? "${dateTime.year.toString().padLeft(4, '0')}-${dateTime.month.toString().padLeft(2, '0')}-${dateTime.day.toString().padLeft(2, '0')}T${dateTime.hour.toString().padLeft(2, '0')}:${dateTime.minute.toString().padLeft(2, '0')}:${dateTime.second.toString().padLeft(2, '0')}Z"
      : null;
}

ResponseApi paginateOdata(
    {@required int count,
    @required int currentPage,
    @required int skip,
    @required int top,
    @required List<dynamic> newListData,
    @required List<dynamic> oldListData}) {
  int totalPage = roundValueUp(count / 10);
  bool nextPage = false;
  if (totalPage > 0) {
    nextPage = currentPage < totalPage ? true : false;
  }
  
  final skipValue= nextPage ? currentPage * top : skip;

  if (oldListData!=null && oldListData.length > 0 && skipValue!=skip) {
    oldListData.addAll(newListData);
  }
  return ResponseApi(
      isSuccess: true,
      result: newListData,
      // result: oldListData!=null && oldListData.length > 0 ? oldListData:newListData,
      currentPage: nextPage ? currentPage+1: currentPage,
      count: count,
      nextPage: nextPage,
      skip: skipValue);
}

int roundValueUp(double value) {
  if (value <= 0) {
    return 0;
  } else {
    final valor = value.toStringAsFixed(1).split('.')[0];
    var round = valor + '.9';
    return double.parse(round).round();
  }
}