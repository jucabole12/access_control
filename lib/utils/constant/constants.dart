class InputTypeConstant {
  final String value;
  const InputTypeConstant(String value) : value = value;
  static const InputTypeConstant text = InputTypeConstant("text");
  static const InputTypeConstant datetime = InputTypeConstant("datetime");
}

class CrudOperationConstant {
  final String value;
  const CrudOperationConstant(String value) : value = value;
  static const CrudOperationConstant create = CrudOperationConstant('create');
  static const CrudOperationConstant edit = CrudOperationConstant('edit');
}

class ApiOperatorsConstant {
  final String value;
  const ApiOperatorsConstant(String value) : value = value;
  static const ApiOperatorsConstant count = ApiOperatorsConstant("\$count");
  static const ApiOperatorsConstant top = ApiOperatorsConstant("\$top");
  static const ApiOperatorsConstant skip = ApiOperatorsConstant("\$skip");
}