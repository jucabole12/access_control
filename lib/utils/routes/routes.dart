import 'package:flutter/widgets.dart';
import 'package:registro_personal_cidenet/data/blocs/employee.bloc.dart';
import 'package:registro_personal_cidenet/data/blocs/home.bloc.dart';
import 'package:registro_personal_cidenet/pages/home/home.page.dart';
import 'package:registro_personal_cidenet/pages/personal_register/personal_register.page.dart';
import 'package:registro_personal_cidenet/utils/bloc/bloc_provider.dart';
import 'package:registro_personal_cidenet/utils/routes/routes_name.dart';

class Routes {
  static Map<String, Widget Function(BuildContext)> get loadRoutes => {
        RoutesName.home: (BuildContext context) => BlocProvider<HomeBloc>(
              bloc: HomeBloc(),
              child: HomePage(),
            ),
        RoutesName.register: (context) => BlocProvider<EmployeeBloc>(
              bloc: EmployeeBloc(),
              child: PersonalRegisterPage(),
            ),
      };
}
