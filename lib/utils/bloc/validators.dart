import 'dart:async';

verificateFieldFormValid(TypeValidators typeValidators) {
  return StreamTransformer<String, String>.fromHandlers(
      handleData: (value, sink) {
    Validators.verificateValid(typeValidators, value, sink);
  });
}

class Validators {
  static verificateValid(TypeValidators typeValidators, value, sink) {
    if (typeValidators != null) {
      if (typeValidators.required && [null, 'undefined', ""].contains(value)) {
        sink.addError('Este campo es requerido');
      } else if (typeValidators.maxLength is int &&
          value.length > typeValidators.maxLength &&
          value.length > 0) {
        sink.addError(
            'Debe ser menor a ${typeValidators.maxLength} caracteres');
      } else if (typeValidators.minLength is int &&
          value.length < typeValidators.minLength &&
          value.length > 0) {
        sink.addError(
            'Debe ser mayor a ${typeValidators.minLength} caracteres');
      } else if ((typeValidators.onlyLettersNoAccents ||
              typeValidators.onlyLettersNoAccentsWithSpaces) &&
          ![null, 'undefined', ""].contains(value)) {
        String path = r'^[a-zA-Z]+$';
        if (typeValidators.onlyLettersNoAccentsWithSpaces) {
          path = r'^[a-zA-Z\b\s]+$';
        }
        RegExp regExp = new RegExp(path, caseSensitive: false);
        if (regExp.hasMatch(value)) {
          sink.add(value);
        } else {
          sink.addError('Debe digitar solo letras, sin acentos ni la letra Ñ');
        }
      } else if (typeValidators.alphaNumeric) {
        String path = r'^[a-zA-Z0-9-]+$';
        RegExp regExp = new RegExp(path, caseSensitive: false);
        if (regExp.hasMatch(value)) {
          sink.add(value);
        } else {
          sink.addError('Debe digitar caracteres alfanuméricos');
        }
      }
    } else {
      sink.add(value);
    }
  }
}

class TypeValidators {
  bool required;
  int maxLength;
  int minLength;
  bool onlyLettersNoAccents;
  bool onlyLettersNoAccentsWithSpaces;
  bool alphaNumeric;

  TypeValidators(
      {this.required = false,
      this.maxLength,
      this.minLength,
      this.onlyLettersNoAccents = false,
      this.onlyLettersNoAccentsWithSpaces = false,
      this.alphaNumeric = false});
}
