import 'dart:async';

import 'package:registro_personal_cidenet/utils/bloc/bloc_field.dart';
import 'package:rxdart/rxdart.dart';

class ValidateStreamsHaveData {
  List<bool> errors;
  BehaviorSubject<bool> _controller;

  void listen(List<ValidateStreams> streams,
      {FieldBlocGeneric<bool> onFieldBloc}) {
    _controller = BehaviorSubject<bool>();
    errors = List.generate(
        streams.where((e) => e.initialVerification == true).length,
        (_) => true);
    errors.addAll(List.generate(
        streams.where((e) => e.initialVerification == false).length,
        (_) => false));

    List.generate(streams.length, (int index) {
      return streams[index].stream.listen(
        (data) {
          errors[index] = false;
          final result = _validate();
          if (onFieldBloc != null) {
            onFieldBloc.sink(result);
          }
        },
        onError: (_) {
          errors[index] = true;
          final result = _validate();
          if (onFieldBloc != null) {
            onFieldBloc.sink(result);
          }
        },
      );
    });
  }

  bool _validate() {
    bool hasNoErrors =
        (errors.cast<bool>().firstWhere((e) => e == true, orElse: () => null) ==
            null);
    _controller.sink.add(hasNoErrors);
    return hasNoErrors;
  }

  Stream<bool> get statusStream => _controller.stream;
  bool get statusValue => _controller.value;

  void dispose() {
    _controller?.close();
  }
}

class ValidateStreams {
  Stream stream;
  bool initialVerification;
  ValidateStreams({this.stream, this.initialVerification = true});
}
