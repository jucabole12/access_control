import 'code.response.dart';

class ResponseApi {
  bool isSuccess;
  String message;
  dynamic result;
  String errorType;

  int currentPage;
  bool nextPage;
  int skip;
  int top;
  int count;
  dynamic headers;

  static ResponseApi getResponseStatusCode({statusCode}) {
    if (statusCode > ResultCode.code500) {
      return ResponseApi(
          isSuccess: false, message: "Error en la conexion, con el servidor");
    }

    if (statusCode == ResultCode.code400) {
      return ResponseApi(
          isSuccess: false, message: "Error: argumento de método no válido");
    }

    if (statusCode == ResultCode.code401) {
      return ResponseApi(
            isSuccess: false, message: "Autorizacion invalidada");
    }

    if (statusCode == ResultCode.code404) {
      return ResponseApi(
          isSuccess: false, message: "no se encontraron resultados");
    }

    if (statusCode > ResultCode.code400) {
      return ResponseApi(isSuccess: false, message: "Error desconocido");
    }

    return ResponseApi(isSuccess: true, message: "Ok");
  }

  ResponseApi(
      {this.isSuccess,
      this.message,
      this.result,
      this.errorType,
      this.currentPage = 1,
      this.nextPage = false,
      this.count = 0,
      this.skip = 0,
      this.top = 1000,
      this.headers});
}
