import 'dart:convert';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:registro_personal_cidenet/data/model/server.response.dart';
import 'package:registro_personal_cidenet/utils/api/server.response.dart';
import 'package:registro_personal_cidenet/utils/constant/constants.dart';
import 'package:registro_personal_cidenet/widgets/alerts/alerts.widget.dart';

class ApiService {
  static ApiService _instancia;

  String mainRoute = '192.168.10.15';
  String port = '5000';

  factory ApiService() {
    if (_instancia == null) {
      _instancia = new ApiService._internal();
    }
    return _instancia;
  }

  ApiService._internal();

  Map<String, String> getHeaderContentType() {
    Map<String, String> headers = {
      'Content-type': 'application/json',
    };
    return headers;
  }

  Future<ResponseApi> getApiOdata(
      {@required String api,
      int top,
      int skip,
      bool count}) async {
    var client = http.Client();
    Map<String, String> queryParams = {};
    top != null
        ? queryParams["${ApiOperatorsConstant.top.value}"] = "$top"
        : null;
    skip != null
        ? queryParams["${ApiOperatorsConstant.skip.value}"] = "$skip"
        : null;
    count != null
        ? queryParams["${ApiOperatorsConstant.count.value}"] = "$count"
        : null;
    String query = Uri(queryParameters: queryParams).query;
    final url = Uri.decodeComponent(
        "http://$mainRoute:$port/odata/accesscontrol/$api${![null, ""].contains(query) ? '?' + query : ''}");
    try {
      Response response = await client.get(Uri.parse(url));
      final responseStatusCode = ResponseApi.getResponseStatusCode(
          statusCode: response.statusCode);
      if (!responseStatusCode.isSuccess) {
        return ResponseApi(isSuccess: false, message: response.body.toString());
      }
      final decodedResp = json.decode(response.body);
      return ResponseApi(
          result: ServerResponseOdata.fromJson(decodedResp),
          isSuccess: true,
          message: "Ok",
          headers: response.headers);
    } catch (e) {
      return ResponseApi(
          isSuccess: false,
          result: e,
          message: 'Response Error : ${e.toString()}');
    } finally {
      client.close();
    }
  }

  Future<ResponseApi> postApiOdata({@required String api,@required Map<String, dynamic> body,String expand,  authToken:false}) async {
    var client = http.Client();
    Map<String, String> queryParams = {};
    String query = Uri(queryParameters: queryParams).query;
    final url = Uri.decodeComponent("http://$mainRoute:$port/odata/accesscontrol/$api${![null,""].contains(query)?'?'+query:''}");
    try {
      body["id"] = 0;
      var _toSend = json.encode(body);
      Response response = await http.post(Uri.parse(url), headers: getHeaderContentType(),body: _toSend);
      final responseStatusCode = ResponseApi.getResponseStatusCode(
          statusCode: response.statusCode);
      if (!responseStatusCode.isSuccess) {
        return ResponseApi(isSuccess: false, message: response.body.toString(), result:response.body );
      }
      final decodedResp = json.decode(response.body);
      return ResponseApi(
          result: decodedResp,
          isSuccess: true,
          message: "Ok",
          headers: response.headers);
    } catch (e) {
      return ResponseApi(
          isSuccess: false,
          result: e,
          message: 'Response Error : ${e.toString()}');
    } finally {
      client.close();
    }
  }

  Future<ResponseApi> patchApiOdata({@required String api, @required dynamic id,@required Map<String, dynamic> body}) async {
    var client = http.Client();

    String url = Uri.decodeComponent("http://$mainRoute:$port/odata/accesscontrol/$api($id)");

    try {
      body['id'] = id;
      var _toSend = json.encode(body);
      
      Response response = await http.patch(Uri.parse(url), headers: getHeaderContentType(),body: _toSend);
      final responseStatusCode = ResponseApi.getResponseStatusCode(
          statusCode: response.statusCode);
      if (!responseStatusCode.isSuccess) {
        return ResponseApi(isSuccess: false, message: response.body.toString(), result:response.body );
      }
      final decodedResp = json.decode(response.body);
      return ResponseApi(
          result: decodedResp,
          isSuccess: true,
          message: "Ok");
    } catch (e) {
      return ResponseApi(
          isSuccess: false,
          result: e,
          message: 'Response Error : ${e.toString()}');
    } finally {
      client.close();
    }
  }

  Future<ResponseApi> delete({@required int id, @required String api}) async {
    var client = http.Client();

    String url = Uri.decodeComponent("http://$mainRoute:$port/odata/accesscontrol/$api($id)");

    try {
      Response response = await http.delete(Uri.parse(url), headers: getHeaderContentType());
      final responseStatusCode = ResponseApi.getResponseStatusCode(
          statusCode: response.statusCode);
      if (!responseStatusCode.isSuccess) {
        return ResponseApi(isSuccess: false, message: response.body.toString(), result:response.body );
      }
      return ResponseApi(
          result: null,
          isSuccess: true,
          message: "Ok");
    } catch (e) {
      return ResponseApi(
          isSuccess: false,
          result: e,
          message: 'Response Error : ${e.toString()}');
    } finally {
      client.close();
    }
  }
}
