class ResultCode {
  static int code200 = 200;
  static int code201 = 201;
  static int code204 = 204;
  static int code400 = 400;
  static int code401 = 401;
  static int code404 = 404;
  static int code500 = 500;
}
