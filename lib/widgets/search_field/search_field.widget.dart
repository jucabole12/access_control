import 'package:flutter/material.dart';
import 'package:registro_personal_cidenet/utils/utils.dart';

class SearchFieldWidget extends StatelessWidget {
  SearchFieldWidget({
    this.stream,
    this.sink,
    this.onChanged,
    this.onClear,
  });

  final Function onChanged;
  final Function onClear;
  final Stream<String> stream;
  final Function(String) sink;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40.0,
      child: _createTextFormField(context),
    );
  }

  Widget _createTextFormField(BuildContext context) {
    return StreamBuilder(
      stream: stream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        TextEditingController controllerSearch = TextEditingController();
        controllerSearch.text =
            (snapshot.data != null && snapshot.data != '') ? snapshot.data : '';
        controllerSearch.value = controllerSearch.value.copyWith(
          text: controllerSearch.text,
          selection: TextSelection(
              baseOffset: controllerSearch.text.length,
              extentOffset: controllerSearch.text.length),
          composing: TextRange.empty,
        );

        return TextFormField(
          controller: controllerSearch,
          autofocus: false,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            suffixIcon: controllerSearch.text.length > 0
                ? Container(
                    child: IconButton(
                      color: Colors.black,
                      disabledColor: Colors.grey,
                      focusColor: Colors.black,
                      onPressed: () {
                        controllerSearch.clear();
                        if (onClear != null) {
                          onClear();
                        }
                        if (sink != null) {
                          sink(null);
                        }
                        hideKeyBoard(context);
                      },
                      icon: Icon(Icons.clear),
                    ),
                  )
                : null,
            prefixIcon: IconButton(
              onPressed: () {},
              color: Colors.black,
              padding: EdgeInsets.all(0.0),
              disabledColor: Colors.grey,
              focusColor: Colors.black,
              icon: Icon(Icons.search, color: Colors.white),
            ),
            border: new OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 0.0),
              borderRadius: const BorderRadius.all(
                const Radius.circular(20.0),
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 1.0),
              borderRadius: const BorderRadius.all(
                const Radius.circular(20.0),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 1.0),
              borderRadius: const BorderRadius.all(
                const Radius.circular(20.0),
              ),
            ),
            contentPadding: EdgeInsets.all(8.0), //here your padding
            filled: true,
            hintText: 'Buscar',
            fillColor: Colors.white24,
            hintStyle: TextStyle(color: Colors.white, fontSize: 15),
          ),
          style: TextStyle(color: Colors.white),
          onChanged: (query) {
            if (onChanged != null) {
              onChanged(query);
            }
            if (sink != null) {
              sink(query);
            }
          },
        );
      },
    );
  }
}
