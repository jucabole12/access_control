import 'package:flutter/material.dart';
import 'package:registro_personal_cidenet/utils/utils.dart';

class Item {
  const Item({this.name, this.value});
  final String name;
  final dynamic value;
}

class DropdownWidget extends StatelessWidget {
  DropdownWidget({
    @required this.labelText,
    @required this.stream,
    @required this.sink,
    @required this.value,
    @required this.listItems,
    this.hintText,
    this.icon,
    this.readOnly = false,
    this.isRequired = false,
    this.onChanged,
  });

  final String labelText;
  final dynamic stream;
  final Function sink;
  final dynamic value;
  final String hintText;
  final IconData icon;
  final bool readOnly;
  final bool isRequired;
  final List<Item> listItems;
  final Function onChanged;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(bottom: 5.0, top: 5.0),
        child: StreamBuilder(
          stream: stream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return DropdownButtonFormField<Item>(
              isExpanded: true,
              itemHeight: 50.0,
              decoration: InputDecoration(
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 20.0),
                  errorText:
                      snapshot.error != null ? snapshot.error.toString() : null,
                  labelStyle: TextStyle(fontSize: 18.0, color: Colors.grey),
                  labelText: isRequired ? "* $labelText" : labelText,
                  hintText: isRequired ? "* $hintText" : hintText,
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10.0)),
                  errorBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10.0)),
                  focusedErrorBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10.0)),
                  errorStyle: TextStyle(color: Colors.red),
                  isDense: false,
                  suffixIconConstraints:
                      BoxConstraints(minWidth: 32, minHeight: 32)),
              value: (snapshot.hasData && snapshot.data == null ||
                      snapshot.hasError)
                  ? null
                  : (snapshot.hasData
                      ? listItems.cast<Item>().firstWhere(
                          (e) => e?.value == snapshot.data,
                          orElse: () => null)
                      : listItems.cast<Item>().firstWhere(
                          (e) => e?.value == this.value,
                          orElse: () => null)),
              icon: Icon(Icons.arrow_drop_down),
              iconSize: 25,
              iconDisabledColor: Colors.grey,
              isDense: true,
              iconEnabledColor: Colors.grey,
              onTap: () {
                hideKeyBoard(context);
              },
              onChanged: (Item item) {
                if (this.sink != null) {
                  this.sink(item.value);
                }
                if(this.onChanged != null){
                  this.onChanged(item.value);
                }
              },
              items: listItems.map((Item item) {
                return DropdownMenuItem<Item>(
                  value: item,
                  child: Text(
                    item.name,
                    style: TextStyle(color: Colors.black),
                  ),
                );
              }).toList(),
            );
          },
        ));
  }
}
