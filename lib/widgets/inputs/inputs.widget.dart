import 'package:flutter/material.dart';

class InputsWidget {
  static datePicker(BuildContext context,
      {Function sink, dynamic value, Function onChange}) async {
    DateTime now = DateTime.now();
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: value != null ? value : DateTime.now(),
      firstDate: DateTime(now.year, now.month - 1, now.day),
      lastDate: now,
    );
    if (picked != null) {
      if (sink != null) {
        sink(picked);
      }
      if (onChange != null) {
        onChange(picked);
      }
    }
  }

  static Widget inputOnlyRead(String value) {
    return Container(
      height: 40.0,
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.transparent,
      ),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            value,
            style: TextStyle(color: Colors.grey, fontSize: 16),
          ),
        ),
      ),
    );
  }
}
