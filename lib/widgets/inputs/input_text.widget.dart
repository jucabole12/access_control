import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:registro_personal_cidenet/utils/bloc/bloc_generic.dart';
import 'package:registro_personal_cidenet/utils/constant/constants.dart';
import 'package:registro_personal_cidenet/utils/utils.dart';
import 'package:registro_personal_cidenet/widgets/inputs/inputs.widget.dart';

class GenericTextFormField extends StatelessWidget {
  GenericTextFormField(
      {this.isRequired = false,
      @required this.labelText,
      @required this.stream,
      @required this.sink,
      @required this.value,
      this.hintText,
      this.inputType = InputTypeConstant.text,
      this.readOnly = false,
      this.maxLength,
      this.maxLines = 1,
      this.floatingLabelBehavior = FloatingLabelBehavior.auto,
      this.fontSizeHintText = 18.0,
      this.fontSizeText = 16.0,
      this.borderColor,
      this.inputFormatters,
      this.onChanged});
  final dynamic stream;
  final Function sink;
  final dynamic value;
  final String labelText;
  final String hintText;
  final InputTypeConstant inputType;
  final bool readOnly;
  final int maxLines;
  final int maxLength;
  final bool isRequired;
  final Bloc<bool> focusNode = Bloc();
  final FloatingLabelBehavior floatingLabelBehavior;
  final TextEditingController _controller = TextEditingController();
  final double fontSizeHintText;
  final double fontSizeText;
  final Color borderColor;
  final List<TextInputFormatter> inputFormatters;
  final Function onChanged;

  @override
  Widget build(BuildContext context) {
    return textFormFieldNormal(context, _controller);
  }

  Widget textFormFieldNormal(
      BuildContext context, TextEditingController _controller) {
    final valueFinal = inputType == InputTypeConstant.datetime ? 
      convertDDMMAAAAHHMMSS(this.value ?? DateTime.now()) : this.value != null ? this.value : '';
    return InkWell(
      borderRadius: BorderRadius.circular(10.0),
      hoverColor: Colors.transparent,
      focusColor: Colors.transparent,
      onTap: () {
        if (inputType == InputTypeConstant.datetime) {
          FocusScope.of(context).requestFocus(new FocusNode());
          InputsWidget.datePicker(context,
              sink: sink,
              value: value,
              onChange: (DateTime value) => {
                    sink(value),
                    _controller.text = value.toString().split(" ")[0]
                  });
        }
      },
      child: Container(
          padding: EdgeInsets.only(bottom: 5.0, top: 5.0),
          child: Focus(
            onFocusChange: (hasFocus) {
              focusNode.sink(hasFocus);
            },
            child: StreamBuilder(
              stream: focusNode.stream,
              builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                return StreamBuilder(
                  stream: stream,
                  builder: (BuildContext context, AsyncSnapshot<dynamic> snap) {
                    return TextFormField(
                      inputFormatters: inputFormatters,
                      onSaved: (_) => FocusScope.of(context).nextFocus(),
                      controller:
                          verificateEnabledController(valueFinal, _controller),
                      readOnly: this.readOnly,
                      enabled: this.readOnly ? false : true,
                      keyboardType: getKeyboardType(inputType),
                      maxLength: this.maxLength,
                      initialValue: verificateEnabledController(
                                  valueFinal, _controller) ==
                              null
                          ? valueFinal
                          : null,
                      scrollPadding: EdgeInsets.fromLTRB(20, 20, 20, 133),
                      textCapitalization: TextCapitalization.sentences,
                      style: textStyleInputs(),
                      decoration: getInputDecoration(snapshot.data, snap.error),
                      autofocus: false,
                      textAlign: TextAlign.left,
                      enableInteractiveSelection: true,
                      onChanged: (String value) {
                        if (this.onChanged != null) {
                          this.onChanged();
                        }
                        this.sink(value);
                      },
                    );
                  },
                );
              },
            ),
          )),
    );
  }

  String getInitialValueFormat() {
    if (this.inputType == InputTypeConstant.datetime) {
      return convertDDMMAAAAHHMMSS(this.value ?? DateTime.now());
    } else {
      return this.value ?? '';
    }
  }

  verificateEnabledController(valueFinal, _controller) {
    if (inputType == InputTypeConstant.datetime || readOnly) {
      _controller.text = valueFinal.toString();
      return _controller;
    } else {
      return null;
    }
  }

  TextStyle textStyleInputs() {
    return readOnly
        ? TextStyle(fontSize: 15.0, color: Colors.black)
        : TextStyle(fontSize: 15.0, color: Colors.black87);
  }

  TextStyle labelStyleInputs(bool focus, dynamic error) {
    return readOnly
        ? TextStyle(fontSize: fontSizeText, color: Colors.grey)
        : TextStyle(
            fontSize: fontSizeText,
            color: focus
                ? (error ?? null) != null
                    ? Colors.red
                    : Colors.grey
                : Colors.grey,
            fontWeight: FontWeight.normal);
  }

  InputDecoration getInputDecoration(bool focus, dynamic error) {
    return InputDecoration(
        errorText: error is String ? error : null,
        labelText: isRequired
            ? "${!["", null].contains(labelText) ? labelText + ' *' : ''}"
            : labelText,
        hintText: isRequired
            ? "${!["", null].contains(hintText) ? hintText + ' ' : ''}"
            : hintText,
        hintStyle: TextStyle(fontSize: fontSizeHintText, color: Colors.grey),
        labelStyle: labelStyleInputs(focus != null ? focus : false, error),
        focusColor: Colors.grey,
        contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 20.0),
        alignLabelWithHint: false,
        floatingLabelBehavior: floatingLabelBehavior,
        fillColor: Colors.white,
        filled: true,
        border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey, width: 1.0),
            borderRadius: BorderRadius.circular(10.0)),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey, width: 1.0),
            borderRadius: BorderRadius.circular(10.0)),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey, width: 1.0),
            borderRadius: BorderRadius.circular(10.0)),
        errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 1.0),
            borderRadius: BorderRadius.circular(10.0)),
        focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 1.0),
            borderRadius: BorderRadius.circular(10.0)),
        errorStyle: TextStyle(color: Colors.red),
        isDense: true,
        disabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey, width: 0.5),
            borderRadius: BorderRadius.circular(10.0)));
  }

  TextInputType getKeyboardType(InputTypeConstant inputType) {
    if (inputType == InputTypeConstant.datetime) {
      return TextInputType.datetime;
    } else {
      return TextInputType.text;
    }
  }
}
