import 'package:flutter/material.dart';

class CardWidget extends StatelessWidget {
  CardWidget({
    @required this.name,
    @required this.area,
    @required this.editFunction,
    @required this.deleteFunction,
  });

  final String name;
  final String area;
  final Function editFunction;
  final Function deleteFunction;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: CircleAvatar(
          backgroundColor: Colors.blue,
          child: Text(
            getInitials(name),
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
            ),
          ),
        ),
        title: Text(name),
        subtitle: Text(area),
        trailing: PopupMenuButton(
          onSelected: (value){
            if(value == 0){
              editFunction();
            }else{
              deleteFunction();
            }
          },
          child: Icon(Icons.more_vert),
          itemBuilder: (context) {
            return [
              PopupMenuItem(
                value: 0,
                child: Row(
                  children: [
                    Icon(Icons.edit, color: Colors.green),
                    SizedBox(width: 2),
                    Text('Editar'),
                  ],
                ),
              ),
              PopupMenuItem(
                value: 1,
                child: Row(
                  children: [
                    Icon(Icons.delete, color: Colors.red),
                    SizedBox(width: 2),
                    Text('Eliminar'),
                  ],
                ),
              )
            ];
          },
        ),
      ),
    );
  }

  String getInitials(String userName) {
    List<String> names = userName.split(" ");
    String initials = "";
    int numWords = 2;

    if (numWords > names.length) {
      numWords = names.length;
    }
    for (var i = 0; i < numWords; i++) {
      initials += '${names[i][0]}';
    }
    return initials.toUpperCase();
  }
}
