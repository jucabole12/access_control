import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

class AlertsWidget {
  static showAlertConfirmation(BuildContext context,
      {double width = 280,
      bool headerAnimationLoop = false,
      @required String title,
      String description = '',
      bool showCloseIcon = true,
      Function(bool) btnOkOnPress,
      String btnOkText = 'Aceptar',
      String btnCancelText = 'Cancelar',
      DialogType dialogType = DialogType.WARNING}) {
    return AwesomeDialog(
      dialogType: dialogType,
      context: context,
      borderSide: BorderSide(color: Colors.green, width: 2),
      buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
      headerAnimationLoop: headerAnimationLoop,
      animType: AnimType.TOPSLIDE,
      title: title,
      desc: description,
      showCloseIcon: showCloseIcon,
      btnOkText: btnOkText,
      btnCancelOnPress: btnOkOnPress != null
          ? () => btnOkOnPress(false)
          : () => Navigator.pop(context),
      btnOkOnPress: () {
        if (btnOkOnPress != null) {
          btnOkOnPress(true);
        }
      },
      btnCancelText: btnCancelText,
      btnOk: ElevatedButton(
        style: ElevatedButton.styleFrom(textStyle: TextStyle(fontSize: 20, color: Colors.white), primary: Colors.green),
        onPressed: () => btnOkOnPress(true),
        child: Text(btnOkText),
      ),
      btnCancel: ElevatedButton(
        style: ElevatedButton.styleFrom(textStyle: TextStyle(fontSize: 20, color: Colors.white), primary: Colors.red),
        onPressed: () => Navigator.pop(context),
        child: Text(btnCancelText),
      ),
    ).show();
  }

  static showAlertOk(BuildContext context, String title,
      {String desc = '',
      DialogType dialogType = DialogType.INFO,
      int autoHideSeconds = 3,
      Function btnOkOnPress}) {
    return AwesomeDialog(
      context: context,
      dialogType: dialogType,
      animType: AnimType.TOPSLIDE,
      title: title,
      desc: desc,
      headerAnimationLoop: false,
      btnOk: ElevatedButton(
        style: ElevatedButton.styleFrom(textStyle: TextStyle(fontSize: 20, color: Colors.white), primary: Colors.green),
        onPressed: () => btnOkOnPress != null ? btnOkOnPress() : Navigator.pop(context),
        child: Text('Ok'),
      )
    )..show();
  }
}