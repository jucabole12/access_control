import 'dart:async';

import 'package:registro_personal_cidenet/data/blocs/home.bloc.dart';
import 'package:registro_personal_cidenet/data/model/employee.model.dart';
import 'package:registro_personal_cidenet/pages/home/home.service.dart';
import 'package:registro_personal_cidenet/pages/personal_register/personal_register.service.dart';
import 'package:registro_personal_cidenet/utils/bloc/bloc_field.dart';
import 'package:registro_personal_cidenet/utils/bloc/bloc_provider.dart';
import 'package:registro_personal_cidenet/utils/bloc/validate_streams.dart';
import 'package:registro_personal_cidenet/utils/bloc/validators.dart';
import 'package:registro_personal_cidenet/data/singletons/employees.singleton.dart';

class EmployeeBloc with Validators implements BlocBase {
  PersonalRegisterService personalRegisterService = PersonalRegisterService();
  HomeService homeService = HomeService();
  HomeBloc homeBloc = HomeBloc();

  EmployeeModel employeeModel;
  static EmployeeBloc _instance;
  factory EmployeeBloc() {
    if(_instance == null){
      _instance = new EmployeeBloc._internal();
      _instance.init();
      _instance.initFormValidate();
    }
    return _instance;
  }
  EmployeeBloc._internal();

  init() {
    surname.stream.listen((event) {
      verificateCorrectEmailFormat();
    });
    firstName.stream.listen((event) {
      verificateCorrectEmailFormat();
    });
    employeeModel = EmployeesSingleton().getEmployeeModel();
    if(employeeModel != null){
      setEmployee(employeeModel);
    }
  }

  void initFormValidate() {
    _validateStreamsHaveData = ValidateStreamsHaveData()
      ..listen([
        ValidateStreams(stream: surname.stream),
        ValidateStreams(stream: secondSurname.stream),
        ValidateStreams(stream: firstName.stream),
        ValidateStreams(stream: otherNames.stream),
        ValidateStreams(stream: idType.stream),
        ValidateStreams(stream: identificationNumber.stream),
        ValidateStreams(stream: email.stream),
        // ValidateStreams(stream: area.stream),
      ]);
  }

  final id = FieldBlocGeneric<int>(defaultValue: 0);
  final surname = FieldBlocGeneric<String>(
      validator: verificateFieldFormValid(TypeValidators(
          required: true,
          onlyLettersNoAccentsWithSpaces: true,
          maxLength: 20)));
  final secondSurname = FieldBlocGeneric<String>(
      validator: verificateFieldFormValid(TypeValidators(
          required: true, onlyLettersNoAccents: true, maxLength: 20)));
  final firstName = FieldBlocGeneric<String>(
      validator: verificateFieldFormValid(TypeValidators(
          required: true, onlyLettersNoAccents: true, maxLength: 20)));
  final otherNames = FieldBlocGeneric<String>(
      validator: verificateFieldFormValid(
          TypeValidators(onlyLettersNoAccentsWithSpaces: true, maxLength: 50)));
  final country = FieldBlocGeneric<String>(validator: verificateFieldFormValid(TypeValidators(required: true)));
  final idType = FieldBlocGeneric<String>(
      validator: verificateFieldFormValid(TypeValidators(required: true)));
  final identificationNumber = FieldBlocGeneric<String>(
      validator: verificateFieldFormValid(
          TypeValidators(required: true, alphaNumeric: true, maxLength: 20)));
  final email = FieldBlocGeneric<String>();
  final admissionDate = FieldBlocGeneric<DateTime>(defaultValue: DateTime.now());
  final area = FieldBlocGeneric<String>(
      validator: verificateFieldFormValid(TypeValidators(required: true)));
  final state = FieldBlocGeneric<String>(defaultValue: 'Activo');
  final registrationDate =
      FieldBlocGeneric<DateTime>(defaultValue: DateTime.now());
  final searchEmployee = FieldBlocGeneric<String>();
  final listSavedEmployees = FieldBlocGeneric<List<EmployeeModel>>();
  ValidateStreamsHaveData _validateStreamsHaveData;
  Stream<bool> get formValidStream =>
      _validateStreamsHaveData?.statusStream;

  EmployeeModel getEmployee() {
    return EmployeeModel(
      id: id.value,
      surname: surname.value,
      secondSurname: secondSurname.value,
      firstName: firstName.value,
      otherNames: otherNames.value,
      country: country.value,
      idType: idType.value,
      identificationNumber: identificationNumber.value,
      email: email.value,
      admissionDate: admissionDate.value,
      area: area.value,
      state: state.value,
      registrationDate: registrationDate.value,
    );
  }

  void setEmployee(EmployeeModel model) {
    id?.sink(model?.id);
    surname?.sink(model?.surname);
    secondSurname?.sink(model?.secondSurname);
    firstName?.sink(model?.firstName);
    otherNames?.sink(model?.otherNames);
    country?.sink(model?.country);
    idType?.sink(model?.idType);
    identificationNumber?.sink(model?.identificationNumber);
    email?.sink(model?.email);
    admissionDate?.sink(model?.admissionDate);
    area?.sink(model?.area);
    state?.sink(model?.state);
    registrationDate?.sink(model?.registrationDate);
  }

  verificateCorrectEmailFormat() {
    String emailFormat = '';
    if (firstName.value != null &&
        surname.value != null &&
        country.value != null) {
      String compoundSurname =
          surname.value.replaceAll(new RegExp(r"\s+\b|\b\s"), "").toLowerCase();
      int countSimilarData = 0;
      emailFormat = '${firstName.value.toLowerCase()}.$compoundSurname${getEmailDomain(country.value)}';
      List<EmployeeModel> lEmployees =
        EmployeesSingleton().getListEmployeesModel();
      if(lEmployees != null){
        countSimilarData = lEmployees.where((element) =>
          element.email == emailFormat).length;
      }
      emailFormat = countSimilarData > 0 ? 
        '${firstName.value.toLowerCase()}.$compoundSurname.$countSimilarData${getEmailDomain(country.value)}' :
        emailFormat;
    }
    email.sink(emailFormat);
  }

  String getEmailDomain(String value) {
    if (value == 'Colombia') {
      return '@cidenet.com.co';
    }
    return '@cidenet.com.us';
  }

  @override
  void dispose() {
    surname.dispose();
    secondSurname.dispose();
    firstName.dispose();
    otherNames.dispose();
    country.dispose();
    idType.dispose();
    identificationNumber.dispose();
    email.dispose();
    admissionDate.dispose();
    area.dispose();
    state.dispose();
    registrationDate.dispose();
    _validateStreamsHaveData?.dispose();
    listSavedEmployees.dispose();
    EmployeesSingleton().destroy();
    _instance = null;
  }
}
