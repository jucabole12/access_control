import 'dart:async';

import 'package:registro_personal_cidenet/data/blocs/employee.bloc.dart';
import 'package:registro_personal_cidenet/data/model/employee.model.dart';
import 'package:registro_personal_cidenet/pages/home/home.service.dart';
import 'package:registro_personal_cidenet/utils/api/server.response.dart';
import 'package:registro_personal_cidenet/utils/bloc/bloc_field.dart';
import 'package:registro_personal_cidenet/utils/bloc/bloc_provider.dart';

class HomeBloc implements BlocBase {
  HomeService homeService = HomeService();

  static HomeBloc _instance;
  factory HomeBloc() {
    if(_instance == null){
      _instance = new HomeBloc._internal();
      _instance.init();
    }
    return _instance;
  }
  HomeBloc._internal();

  init() {
    searchEmployee.bloc.stream.listen((event) {
      onSearchChangedDebounce(event);
    });
    homeService.getEmployeeList(this);
  }

  final searchEmployee = FieldBlocGeneric<String>(defaultValue: '');
  final employeeList = FieldBlocGeneric<ResponseApi>(defaultValue: ResponseApi());
  final listSavedEmployees = FieldBlocGeneric<List<EmployeeModel>>();
  final isLoading = FieldBlocGeneric<bool>(defaultValue: false);

  Timer _debounce;
  onSearchChangedDebounce(String query) {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(seconds: 1), () {
      print('SEARCH =======================================> :::: $query');
      homeService.searchEmployee(
        query,
        this,
      );
    });
  }

  @override
  void dispose() {
    searchEmployee.dispose();
    listSavedEmployees.dispose();
  }
}
