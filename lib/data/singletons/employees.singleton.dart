import 'package:registro_personal_cidenet/data/model/employee.model.dart';

class EmployeesSingleton {
  static final EmployeesSingleton _singleton =
      EmployeesSingleton._internal();

  List<EmployeeModel> _employees;
  EmployeeModel _employee;

  factory EmployeesSingleton() {
    return _singleton;
  }

  EmployeesSingleton._internal();

  List<EmployeeModel> getListEmployeesModel() {
    return _employees;
  }

  EmployeeModel getEmployeeModel() {
    return _employee;
  }

  void setListEmployeesSingleton(
      List<EmployeeModel> employees) {
    _employees = employees;
  }

  void setEmployeeSingleton(
      EmployeeModel employee) {
    _employee = employee;
  }

  void destroy() {
    _employees = null;
    _employee = null;
  }
}
