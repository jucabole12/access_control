
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class ServerResponseOdata {
  List<Map<String, dynamic>> value;
  @JsonKey(name: '@odata.count')
  int count;

  ServerResponseOdata({@required this.value, this.count});
  factory ServerResponseOdata.fromJson(Map<String, dynamic> json) {
    return ServerResponseOdata(
      value: (json['value'] as List<dynamic>)
          .map((e) => e as Map<String, dynamic>)
          .toList(),
      count: json['@odata.count'] as int,
    );
  }
}
