import 'package:registro_personal_cidenet/utils/utils.dart';

class EmployeeModel {
  int id;
  String surname;
  String secondSurname;
  String firstName;
  String otherNames;
  String country;
  String idType;
  String identificationNumber;
  String email;
  DateTime admissionDate;
  String area;
  String state;
  DateTime registrationDate;

  EmployeeModel({
    this.id,
    this.surname,
    this.secondSurname,
    this.firstName,
    this.otherNames,
    this.country,
    this.idType,
    this.identificationNumber,
    this.email,
    this.admissionDate,
    this.area,
    this.state,
    this.registrationDate,
  });

  Map<String, dynamic> toJSON() => {
        "id": null,
        "surname": surname,
        "secondSurname": secondSurname,
        "firstName": firstName,
        "otherNames": otherNames,
        "country": country,
        "idType": idType,
        "identificationNumber": identificationNumber,
        "email": email,
        "admissionDate": convertAAAAMMDDTZ(admissionDate),
        "area": area,
        "state": state,
        "registrationDate": convertAAAAMMDDTZ(registrationDate),
      };

  EmployeeModel fromJson(Map<String, dynamic> json) {
    return EmployeeModel(
      id: json["id"],
      surname: json["surname"],
      secondSurname: json["secondSurname"],
      firstName: json["firstName"],
      otherNames: json["otherNames"],
      country: json["country"],
      idType: json["idType"],
      identificationNumber: json["identificationNumber"],
      email: json["email"],
      admissionDate: DateTime.parse(json["admissionDate"]),
      area: json["area"],
      state: json["state"],
      registrationDate: DateTime.parse(json["registrationDate"]),
    );
  }

  List<EmployeeModel> toListFromJson(List<Map<String, dynamic>> str) {
    return List<EmployeeModel>.from(
        str.map((x) => EmployeeModel().fromJson(x)));
  }
}
