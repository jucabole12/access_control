import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:registro_personal_cidenet/data/blocs/employee.bloc.dart';
import 'package:registro_personal_cidenet/utils/bloc/bloc_provider.dart';
import 'package:registro_personal_cidenet/utils/constant/constants.dart';
import 'package:registro_personal_cidenet/utils/uppercase_text_formatter.dart';
import 'package:registro_personal_cidenet/utils/utils.dart';
import 'package:registro_personal_cidenet/widgets/alerts/alerts.widget.dart';
import 'package:registro_personal_cidenet/widgets/dropdown/dropdown.widget.dart';
import 'package:registro_personal_cidenet/widgets/inputs/input_text.widget.dart';
import 'package:registro_personal_cidenet/widgets/inputs/inputs.widget.dart';

class PersonalRegisterPage extends StatelessWidget {
  PersonalRegisterPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final EmployeeBloc bloc = BlocProvider.ofBloc<EmployeeBloc>(context);
    return Scaffold(
      appBar: AppBar(title: Text('Registro de empleado')),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(
            top: 15.0,
            right: 15.0,
            left: 15.0,
            bottom: 70.0,
          ),
          child: Column(
            children: [
              Container(
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.grey),
                child: Padding(
                  padding: const EdgeInsets.all(7.0),
                  child: Icon(
                    Icons.person_add,
                    color: Colors.white,
                    size: 50,
                  ),
                ),
              ),
              SizedBox(height: 10),
              GenericTextFormField(
                sink: bloc.surname.sink,
                stream: bloc.surname.stream,
                value: bloc.surname.value,
                labelText: 'Primer apellido',
                inputFormatters: [UpperCaseTextFormatter()],
              ),
              SizedBox(height: 10),
              GenericTextFormField(
                sink: bloc.secondSurname.sink,
                stream: bloc.secondSurname.stream,
                value: bloc.secondSurname.value,
                labelText: 'Segundo apellido',
                inputFormatters: [UpperCaseTextFormatter()],
              ),
              SizedBox(height: 10),
              GenericTextFormField(
                sink: bloc.firstName.sink,
                stream: bloc.firstName.stream,
                value: bloc.firstName.value,
                labelText: 'Primer nombre',
                inputFormatters: [UpperCaseTextFormatter()],
              ),
              SizedBox(height: 10),
              GenericTextFormField(
                sink: bloc.otherNames.sink,
                stream: bloc.otherNames.stream,
                value: bloc.otherNames.value,
                labelText: 'Otros nombres',
                inputFormatters: [UpperCaseTextFormatter()],
              ),
              SizedBox(height: 10),
              DropdownWidget(
                listItems: countryList,
                stream: bloc.country.stream,
                sink: bloc.country.bloc.sink,
                value: bloc.country.value,
                labelText: 'País del empleo',
                isRequired: true,
                hintText: 'Campo requerido',
                onChanged: (value){
                  bloc.verificateCorrectEmailFormat();
                },
              ),
              SizedBox(height: 10),
              DropdownWidget(
                listItems: idTypeList,
                stream: bloc.idType.stream,
                sink: bloc.idType.sink,
                value: bloc.idType.value,
                labelText: 'Tipo de identificación',
                isRequired: true,
                hintText: 'Campo requerido',
                onChanged: (value) {
                  bloc.idType.sink('prueba');
                }
              ),
              SizedBox(height: 10),
              GenericTextFormField(
                sink: bloc.identificationNumber.sink,
                stream: bloc.identificationNumber.stream,
                value: bloc.identificationNumber.value,
                labelText: 'Número de identificación',
              ),
              SizedBox(height: 10),
              StreamBuilder(
                stream: bloc.email.stream,
                builder:
                    (BuildContext context, AsyncSnapshot<String> snapshot) {
                  return GenericTextFormField(
                    sink: bloc.email.sink,
                    stream: bloc.email.stream,
                    value: bloc.email.value,
                    labelText: 'Correo electrónico',
                    readOnly: true,
                  );
                },
              ),
              SizedBox(height: 10),
              StreamBuilder(
                stream: bloc.admissionDate.stream,
                builder: (BuildContext context, AsyncSnapshot<DateTime> snapshot) {
                  return GenericTextFormField(
                    sink: bloc.admissionDate.sink,
                    stream: bloc.admissionDate.stream,
                    value: snapshot.hasData ? bloc.admissionDate.value : DateTime.now(),
                    labelText: 'Fecha de ingreso',
                    inputType: InputTypeConstant.datetime,
                    readOnly: true,
                  );
                },
              ),
              SizedBox(height: 10),
              DropdownWidget(
                listItems: areaList,
                stream: bloc.area.stream,
                sink: bloc.area.sink,
                value: bloc.area.value,
                labelText: 'Área',
                isRequired: true,
                hintText: 'Campo requerido',
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  Expanded(
                    flex: 5,
                    child: Text(bloc.id.value != null ? 'Fecha y hora de edición' : 'Fecha y hora de registro'),
                  ),
                  SizedBox(width: 5),
                  Expanded(child: Text('Estado'))
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: StreamBuilder(
                      stream: bloc.registrationDate.stream,
                      builder: (BuildContext context,
                          AsyncSnapshot<DateTime> snapshot) {
                        return InputsWidget.inputOnlyRead(
                            snapshot.hasData ? (convertDDMMAAAAHHMMSS(snapshot.data)).toString() : '');
                      },
                    ),
                  ),
                  SizedBox(width: 5),
                  AbsorbPointer(
                    child: Switch(
                      onChanged: (value) {},
                      value: true,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final response = await bloc.personalRegisterService.createOrUpdateEmployeeRegister(bloc);
          if(!response.isSuccess){
            await AlertsWidget.showAlertOk(context, 'Error al crear el empleado', dialogType: DialogType.ERROR);
            return;
          }
          await AlertsWidget.showAlertOk(
            context, 
            'Empleado creado exitosamente', 
            dialogType: DialogType.SUCCES,
            btnOkOnPress: () {
              bloc.homeService.getEmployeeList(bloc.homeBloc);
              Navigator.pop(context);
              Navigator.pop(context);
            }
          );
        },
        tooltip: 'Save',
        child: Icon(Icons.save),
      ),
    );
  }
}
