import 'dart:async';
import 'package:registro_personal_cidenet/data/blocs/employee.bloc.dart';
import 'package:registro_personal_cidenet/utils/api/api.service.dart';
import 'package:registro_personal_cidenet/utils/api/server.response.dart';
import 'package:registro_personal_cidenet/utils/bloc/bloc_provider.dart';

class PersonalRegisterService implements ServiceBase {
  ApiService apiService = ApiService();

  static PersonalRegisterService _instance;

  factory PersonalRegisterService() {
    if (_instance == null) {
      _instance = new PersonalRegisterService._internal();
    }
    return _instance;
  }
  PersonalRegisterService._internal();

  Future<ResponseApi> createOrUpdateEmployeeRegister(EmployeeBloc employeeBloc) async {
    ResponseApi responseEmployeeRegister;
    final employeToCreate = employeeBloc.getEmployee();
    if(employeeBloc.id.value != null && employeeBloc.id.value > 0){
      responseEmployeeRegister = await this.apiService.patchApiOdata(
        api: 'Employees', 
        id: employeeBloc.id.value,
        body: employeToCreate.toJSON(),
      );
    } else {
      responseEmployeeRegister = await this.apiService.postApiOdata(
        api: 'Employees', 
        body: employeToCreate.toJSON(),
      );
    }
    return responseEmployeeRegister;
  }

  @override
  void dispose() {
    _instance = null;
  }
}