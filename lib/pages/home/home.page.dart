import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:registro_personal_cidenet/data/blocs/home.bloc.dart';
import 'package:registro_personal_cidenet/data/model/employee.model.dart';
import 'package:registro_personal_cidenet/data/singletons/employees.singleton.dart';
import 'package:registro_personal_cidenet/utils/api/server.response.dart';
import 'package:registro_personal_cidenet/utils/bloc/bloc_provider.dart';
import 'package:registro_personal_cidenet/utils/routes/routes_name.dart';
import 'package:registro_personal_cidenet/widgets/alerts/alerts.widget.dart';
import 'package:registro_personal_cidenet/widgets/card/card.widget.dart';
import 'package:registro_personal_cidenet/widgets/search_field/search_field.widget.dart';

class HomePage extends StatelessWidget {
  HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final HomeBloc bloc = BlocProvider.ofBloc<HomeBloc>(context);
    List<EmployeeModel> listEmployees = <EmployeeModel>[];
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            Image.asset('assets/cidenet.png', width: 50,),
            SizedBox(width: 8.0,),
            Expanded(
              child: SearchFieldWidget(
                stream: bloc.searchEmployee.stream,
                sink: bloc.searchEmployee.sink,
              ),
            ),
          ],
        ),
      ),
      body: StreamBuilder(
        stream: bloc.isLoading.stream,
        builder: (BuildContext context, AsyncSnapshot<bool> snapshotLoading) {
          if(snapshotLoading.hasData && snapshotLoading.data){
            return Center(child: CircularProgressIndicator());
          }
          return StreamBuilder(
            stream: bloc.employeeList.stream,
            builder: (BuildContext context, AsyncSnapshot<ResponseApi> snapshot) {
              if(snapshot.hasData){
                if(snapshot.data.count <= 0){
                  return Container(child: Center(child: Text('No hay registros', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),)));
                }
                return Container(
                  height: double.infinity,
                  child: ListView.builder(
                    itemCount: snapshot.data.count,
                    itemBuilder: (context, index) {
                      return CardWidget(
                        deleteFunction: () async {
                          await AlertsWidget.showAlertConfirmation(
                            context, 
                            title: '¿Está seguro de que desea eliminar el empleado?',
                            btnCancelText: 'No',
                            btnOkText: 'Sí',
                            btnOkOnPress: (value) async {
                              if(value){
                                final response = await bloc.homeService.deleteEmployee(snapshot.data.result[index].id);
                                if(response.isSuccess){
                                  bloc.homeService.getEmployeeList(bloc);
                                }
                                Navigator.pop(context);
                              }
                            },
                          );
                        },
                        editFunction: () {
                          EmployeesSingleton().setEmployeeSingleton(snapshot.data.result[index]);
                          Navigator.pushNamed(context, RoutesName.register);
                        },
                        area: snapshot.data.result[index].area,
                        name:
                            '${snapshot.data.result[index].firstName} ${snapshot.data.result[index].surname} ${snapshot.data.result[index].secondSurname}',
                      );
                    }),
                );
              }else{
                return Center(child: CircularProgressIndicator());
              }
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, RoutesName.register);
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
