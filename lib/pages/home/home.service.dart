import 'package:registro_personal_cidenet/data/blocs/home.bloc.dart';
import 'package:registro_personal_cidenet/data/model/employee.model.dart';
import 'package:registro_personal_cidenet/data/model/server.response.dart';
import 'package:registro_personal_cidenet/data/singletons/employees.singleton.dart';
import 'package:registro_personal_cidenet/utils/api/api.service.dart';
import 'package:registro_personal_cidenet/utils/api/server.response.dart';
import 'package:registro_personal_cidenet/utils/bloc/bloc_provider.dart';
import 'package:registro_personal_cidenet/utils/utils.dart';

class HomeService implements ServiceBase {
  ApiService _apiService = ApiService();

  static HomeService _instance;

  factory HomeService() {
    if (_instance == null) {
      _instance = new HomeService._internal();
    }
    return _instance;
  }
  HomeService._internal();

  Future<void> searchEmployee(String query, HomeBloc bloc) async {
    if (!["", null].contains(query)) {
      bloc.employeeList.sink(ResponseApi());
      getEmployeeList(bloc, query: query);
    }else{
      bloc.employeeList.sink(ResponseApi());
      getEmployeeList(bloc, query: null);
    }
  }

  Future<void> getEmployeeList(HomeBloc bloc, {String query}) async {
    bloc.isLoading.sink(true);
    final finalQuery = !["", null].contains(query) ? query : '';
    final api = "Employees/EmployeesService.SearchEmployees(query='$finalQuery')";
    final response = await this._apiService.getApiOdata(
      api: api,
      count: true
    );
    if (response.isSuccess) {
      final odataResult = response.result as ServerResponseOdata;
      final newlistEmployees =
          EmployeeModel().toListFromJson(odataResult.value);
      ResponseApi paginate = paginateOdata(
          count: odataResult.count,
          currentPage: bloc.employeeList.value.currentPage,
          skip: bloc.employeeList.value.skip,
          top: bloc.employeeList.value.top,
          oldListData: bloc.employeeList.value.result,
          newListData:
              newlistEmployees);
      bloc.employeeList.sink(paginate);
      EmployeesSingleton().setListEmployeesSingleton(newlistEmployees);
      bloc.isLoading.sink(false);
    } else {
      bloc.employeeList.sink(ResponseApi(result: <EmployeeModel>[]));
      bloc.isLoading.sink(false);
    }
  }

  Future<ResponseApi> deleteEmployee(int id) async {
    final api = "Employees";
    final response = await this._apiService.delete(
      api: api,
      id: id,
    );
    return response;
  }

  @override
  void dispose() {
    _instance = null;
  }
}
