import 'package:flutter/material.dart';
import 'package:registro_personal_cidenet/utils/routes/routes.dart';
import 'package:registro_personal_cidenet/utils/routes/routes_name.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Registro empleados',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: RoutesName.home,
      routes: Routes.loadRoutes,
    );
  }
}
